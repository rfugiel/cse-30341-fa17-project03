class Chat {
public:
	Chat(std::string host, std::string port);

	std::string		host_;
	std::string		port_;
	std::string		topic_;
	std::string		user_;

	void			login();
	void			chat(Client);
};