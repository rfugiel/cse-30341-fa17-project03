// client.h: PS Client Library -------------------------------------------------

#pragma once	// Ensures the header file is only loaded once by the compiler

#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <semaphore.h>
#include <pthread.h>
#include <queue>
#include <map>
#include <ctime>
#include <cstring>
#include <set>
#include <netdb.h> 
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdexcept>


#include "macros.h"
#include "thread.h"
#include "queue.h"

// Message Structure

typedef struct {
	std::string 	type;        // Message type (MESSAGE, IDENTIFY, SUBSCRIBE, UNSUBSCRIBE, RETRIEVE, DISCONNECT)
	std::string 	topic;       // Message topic
	std::string 	sender;      // Message sender
	size_t      	nonce;       // Sender's nonce
	std::string 	body;        // Message body
} Message;

// --- Callback Class (and derived classes)

class Callback {	// Abstract Class
public:
	Callback();
	virtual void run(Message &m) = 0;
};

class EchoCallback : public Callback {
public:
    void run(Message &m);
};

class ChatCallback : public Callback {
public:
    void run(Message &m);
};

// --- Client Class

class Client {
private:
	bool									shutdown_;
	pthread_mutex_t 						shutdown_lock_;
	Queue<Message>							incoming_messages_;
	Queue<Message>							outgoing_messages_;

public:
	Client(const char *host, const char *port, const char *cid);

	void 			publish(const char *topic, const char *message, size_t length);
	void 			identify();
	void 			subscribe(const char *topic, Callback *callback);
	void 			unsubscribe(const char *topic);
	void 			run();
	void 			disconnect();
	bool 			shutdown();
	void 			set_shutdown();
	Message 		get_incoming();
	Message 		get_outgoing();
	void 			add_outgoing(Message m);
	void 			add_incoming(Message m);

	std::string 									host_;
	std::string 									port_;
	std::string 									cid_;
	size_t											nonce_;
	std::map<std::string, std::set<Callback*>> 		callbacks_;
};

class Socket{
    public:
        Socket();
        FILE *connect_socket(std::string host, std::string port);
};


// server connection functions
std::string get_message_string(Message m);
void *do_send(void *arg);
void *do_receive(void *arg);
void *do_callback(void *arg);





// vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: ----------------------------------
