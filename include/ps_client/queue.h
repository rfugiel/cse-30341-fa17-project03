// --- Thread-Safe Queue (Referenced from CSE-30341: Lecture 11)

template <typename T>
class Queue {
private:
    std::queue<T>   data;
    T		    sentinel;
    pthread_mutex_t lock;
    pthread_cond_t  fill;

public:
    Queue() {
    	int rc;
    	Pthread_mutex_init(&lock, NULL);
    	Pthread_cond_init(&fill, NULL);
    }

    void push(const T &value) {
    	int rc;
    	Pthread_mutex_lock(&lock);

		data.push(value);
    	Pthread_cond_signal(&fill);
    	Pthread_mutex_unlock(&lock);
    }
    
    T pop() {
    	int rc;
    	Pthread_mutex_lock(&lock);
    	while (data.empty()) {
    	    Pthread_cond_wait(&fill, &lock);
		}

		T value = data.front();	//checks if empty (It is then we try to access sentinel)
		data.pop();
		
	    Pthread_mutex_unlock(&lock);
	    return value;
	}
};