typedef void *(*thread_func)(void *);

class Thread {
private:
    pthread_t	thread;
public:
    Thread() {}

    void start(thread_func func, void *arg) {
    	int rc;
    	Pthread_create(&thread, NULL, func, arg);
    }

    void join(void **result) {
    	int rc;
    	Pthread_join(thread, result);
    }

    void detach(){	
    	int rc;
    	Pthread_detach(thread);	
    }
	
};
