#include <ps_client/client.h>
#include <chat/chat.h>
#include <sys/poll.h>

std::string user;
std::string topic;

void *send(void *arg) {
    Client *client = (Client *)arg;
    while(true) {
        std::string myMessage = "";
        std::cin >> myMessage;
        client->publish(user.c_str(), myMessage.c_str(), strlen(myMessage.c_str()));
    }

    return NULL;
}


int main() {
    std::cout << "Enter your desired username: ";
    std::cin >> user;
    std::cout << "Enter a user you would like to chat with: ";
    std::cin >> topic;

    std:: string host = "localhost";
    std::string port = "9411";
    Client client = Client(host.c_str(), port.c_str(), user.c_str());


    Thread sender;
    sender.start(send, (void *)&client);
    sender.detach();

    ChatCallback c;

    client.subscribe(topic.c_str(), &c);
    client.run();


    /*
    struct pollfd fds;
    int ret;
    fds.fd = 0; 
    fds.events = POLLIN;



    
    while(true) {
        ret = poll(&fds, 1, 1000);

        if(ret == 1) {  //If something is in stdin
            std::cin >> myMessage;
            Thread sender;
            sender.start(send, (void *)&client);
            client->set_shutdown();
            sender.detach();
        }else if(ret == 0) {
            //do nothing
        }else{
                printf("Error\n");
        }
    }
    */

    return 0;
}
