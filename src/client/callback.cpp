#include <ps_client/client.h>

Callback::Callback() {} //constructer

void EchoCallback::run(Message &m) {
    fprintf(stdout, "EchoCallback: Received message %s from %s (%zu bytes) \n", m.topic.c_str(), m.sender.c_str(), m.body.length());
}

void  ChatCallback::run(Message &m) {
    fprintf(stdout, "%s: %s \n", m.sender.c_str(), m.body.c_str());
}
