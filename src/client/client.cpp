#include <ps_client/client.h>

/**** --- CLIENT --- ****/

Client::Client(const char *host, const char *port, const char *cid) {
	// Initialize values
	std::string s_host(host, strlen(host));
	std::string s_port(port, strlen(port));
	std::string s_cid(cid, strlen(cid));

	host_ = s_host;
	port_ = s_port;
	cid_ = s_cid;

	shutdown_lock_ = PTHREAD_MUTEX_INITIALIZER;
	shutdown_ = false;

	// Generate nonce
	srand(time(NULL));
	nonce_ = rand();


	Message m;
	m.type = "IDENTIFY";
	m.topic = "";
	m.sender = cid_;
	m.nonce = nonce_;
	m.body = "";

	add_outgoing(m);

}

void Client::identify() {
	Message m;
	m.type = "IDENTIFY";
	m.sender = cid_;
	m.nonce = nonce_;

	outgoing_messages_.push(m);
}

void Client::publish(const char *topic, const char *message, size_t length) {
	std::string s_topic(topic, strlen(topic));
	std::string s_message(message, length);

	Message m;
	m.type = "PUBLISH";
	m.topic = s_topic;
	m.sender = cid_;
	m.nonce = nonce_;
	m.body = s_message;
	
	outgoing_messages_.push(m);
}

void Client::subscribe(const char *topic, Callback *callback) {
	std::string s_topic(topic, strlen(topic));

	Message m;
	m.type = "SUBSCRIBE";
	m.topic = s_topic;
	m.sender = cid_;
	m.nonce = nonce_;
	
	outgoing_messages_.push(m);

	callbacks_[s_topic].insert(callback);
}

void Client::unsubscribe(const char *topic) {
	std::string s_topic(topic, strlen(topic));

	Message m;
	m.type = "UNSUBSCRIBE";
	m.topic = s_topic;
	m.sender = cid_;
	m.nonce = nonce_;

	outgoing_messages_.push(m);
	
	callbacks_.erase(s_topic);
}

void Client::disconnect() {
	Message m;
	m.type = "DISCONNECT";
	m.sender = cid_;
	m.nonce = nonce_;

}

void Client::set_shutdown(){
	int rc;
	Pthread_mutex_lock(&shutdown_lock_);
	shutdown_ = true;
	Pthread_mutex_unlock(&shutdown_lock_);
}

bool Client::shutdown() {
	int rc;
	Pthread_mutex_lock(&shutdown_lock_);
	bool result = shutdown_;
	Pthread_mutex_unlock(&shutdown_lock_);
	return result;
}

void Client::run(){
	Thread  thread_publish;
	Thread  thread_retrieve;
	Thread  thread_cb;

    thread_publish.start(do_send, (void *)this);
    thread_retrieve.start(do_receive, (void *)this);
    thread_cb.start(do_callback, (void *)this);
   	void *result[3];

    thread_publish.join(&result[0]);
    thread_retrieve.join(&result[1]);
    thread_cb.join(&result[2]);
}

Message Client::get_incoming() {
	return incoming_messages_.pop();
}

Message Client::get_outgoing() {
	return outgoing_messages_.pop();
}

void Client::add_incoming(Message m) {
	incoming_messages_.push(m);
}

void Client::add_outgoing(Message m) {
	outgoing_messages_.push(m);
}
