#include <ps_client/client.h>

Socket::Socket() {}

FILE* Socket::connect_socket(std::string host, std::string port){
    struct addrinfo *results;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    int status;
    if ((status = getaddrinfo(host.c_str(), port.c_str(), &hints, &results)) != 0){
        fprintf(stderr, "getaddrinfo failed: %s\n", gai_strerror(status));
    }
    //now try to connect
    int socket_fd = -1;
    for (struct addrinfo *res = results; res != NULL && socket_fd < 0; res = res->ai_next){
        if ((socket_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1){
            fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
            continue;
        }

        if (connect(socket_fd, res->ai_addr, res->ai_addrlen) == -1){
            //fprintf(stderr, "Was not able to connect to %s:%s: %s\n",host.c_str(), port.c_str(), strerror(errno));
            close(socket_fd);
            socket_fd = -1;
            continue;  
        }
    }

    freeaddrinfo(results);

    FILE* socket_file = fdopen(socket_fd, "w+");
    if (socket_file == NULL){
        fprintf(stderr, "Unable to open file: %s\n", strerror(errno));
        close(socket_fd);
        return NULL;
    }


    return socket_file;
}

//get message that willa ctually be sent to server
std::string get_message_string(Message m){
    std::string msg_string;
    if (m.type == "IDENTIFY"){
         msg_string = m.type + " " + m.sender + " " + std::to_string(m.nonce) + "\n";
    }
    else if (m.type == "SUBSCRIBE"){
        msg_string = m.type + " " + m.topic + "\n";

    }
    else if (m.type == "UNSUBSCRIBE"){
        msg_string = m.type + " " + m.topic + "\n";

    }
    else if (m.type == "PUBLISH"){
        msg_string = m.type + " " + m.topic + " " + std::to_string(m.body.size()) + "\n" + m.body;

    }
    else if (m.type == "DISCONNECT"){
        msg_string = m.type + " " + m.sender + " " + std::to_string(m.nonce) + "\n"; 
    }
    else if (m.type == "RETRIEVE"){
        msg_string = m.type + " " + m.sender + "\n";
    }
    else {
            puts("Invalid Message Type");
    }
    return msg_string;

}

void *do_send(void *arg){
    Socket s;
    Client *client = (Client *)arg;
    FILE *socket_file = s.connect_socket(client->host_, client->port_);

    char buffer[BUFSIZ];

    while (!(client->shutdown())) {
        Message m = client->get_outgoing();

        // Send to server.
        if (m.type.compare("IDENTIFY") == 0) {
            fprintf(socket_file, "%s %s %zu\n", m.type.c_str(), m.sender.c_str(), m.nonce);
            fgets(buffer, BUFSIZ, socket_file);
        }
        else if (m.type.compare("PUBLISH") == 0) {
            fprintf(socket_file, "%s %s %zu\n%s", m.type.c_str(), m.topic.c_str(), m.body.length(), m.body.c_str());
            fgets(buffer, BUFSIZ, socket_file);
        }
        else if (m.type.compare("SUBSCRIBE") == 0) {
            fprintf(socket_file, "%s %s\n", m.type.c_str(), m.topic.c_str());
            fgets(buffer, BUFSIZ, socket_file);
        }
        else if (m.type.compare("UNSUBSCRIBE") == 0) {
            fprintf(socket_file, "%s %s\n", m.type.c_str(), m.topic.c_str());
            fgets(buffer, BUFSIZ, socket_file);
        }
        else if (m.type.compare("DISCONNECT") == 0) {
            fprintf(socket_file, "%s %s %zu\n", m.type.c_str(), m.sender.c_str(), m.nonce);
            fgets(buffer, BUFSIZ, socket_file);
        }
        else {
            std::cout << "Error in type: " << m.type << std::endl;
        }
    }
    return NULL;
}

void *do_receive(void *arg){
    Socket s;
    Client *client = (Client *)arg;
    char buffer[BUFSIZ];

    FILE *socket_file = s.connect_socket(client->host_, client->port_);

    fprintf(socket_file, "IDENTIFY %s %zu\n", client->cid_.c_str(), client->nonce_);
    fgets(buffer, BUFSIZ, socket_file); 




    while (!(client->shutdown())) {
        Message m;
        char header[BUFSIZ];
        char body[BUFSIZ];
        char topic[BUFSIZ];
        char sender[BUFSIZ];
        char length[BUFSIZ];


        fprintf(socket_file, "RETRIEVE %s\n", client->cid_.c_str());
        fgets(header, BUFSIZ, socket_file);
        
        sscanf(header, "MESSAGE %s FROM %s LENGTH %s", topic, sender, length);

        fgets(body, atoi(length)+1, socket_file);

        // Update Message struct.
        m.type = "RETRIEVE";  // TODO CHECK
        m.topic = topic;
        m.sender = sender;
        m.nonce = client->nonce_;
        m.body = body;  


        // Push message onto incoming queue.
        client->add_incoming(m);
    }


    return NULL;
}

void *do_callback(void *arg){
    Socket s;
    Client *client = (Client *)arg;
    FILE *socket_file = s.connect_socket(client->host_, client->port_);
    char buffer[BUFSIZ];

    fprintf(socket_file, "IDENTIFY %s %zu\n", client->cid_.c_str(), client->nonce_);
    fgets(buffer, BUFSIZ, socket_file); 

    while (!(client->shutdown())) {
        Message incoming = client->get_incoming();

        try {
        for(Callback *callback: client->callbacks_.at(incoming.topic)){
                callback->run(incoming);
            }
        } catch (std::out_of_range &e) {
            //fprintf(stderr, "There were no callbacks for %s\n", incoming.topic.c_str());
            continue;
        }

    }


    return 0;
}

