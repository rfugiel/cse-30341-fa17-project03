CSE.30341.FA17: Project 03
==========================

This is the documentation for [Project 03] of [CSE.30341.FA17].

Members
-------

1. Radomir Fugiel (rfugiel@nd.edu)

Design
------

> 1. The client library needs to provide a `Client` class.
>
>   - What does the `Client` class need to keep track of?

This class will implement the methods: publish, subscribe, usnubscribe, disconnect , run and shutdown. It will keep track of the server it needs to connect to (host + port). Also the Topic the client wants and the Client ID.

>   - How will the `Client` connect to the server?

Using streaming sockets

>   - How will it implement concurrent publishing, retrieval, and processing of
>     messages?

Multiple threads will be utilized to allow for concurrency.

>   - How will it pass messages between different threads?

This will require the effective use of locks to create a concurrent data structure that multiple threads can access.

>   - What data members need to have access synchronized? What primitives will
>     you use to implement these?

The server socket (File descriptor) as well as the topic queues need to have synchronized access. I plan on using
sempaphores which will limit access.

>   - How will threads know when to block or to quit?

The semaphores will take care of the thread blocking. They will know when to quit if the there is an error or they terminate.
>
>   - How will the `Client` determine which `Callbacks` belong to which topics?

The Callback will be processes by a callback function which looks at stored info.


> 2. The client library needs to provide a `Callback` class.

>   - What does the `Callback` class need to keep track of?

The message that is being sent

>   - How will applications use this `Callback` class?

When a message needs to be processed, callback will be called

> 3. The client library needs to provide a `Message` struct.


>   - What does the `Message` struct need to keep track of?

Message type, topic, sender, nonce and body

>   - What methods would be useful to have in processing `Messages`?

Methods such as addToTopicQueue, or printMessage would be good



Response.

> 4. The client library needs to provide a `Thread` class.
>
>   - What does the `Thread` class need to keep track of?

The thread will need keep track of all the info associated with the calling client.

>   - What POSIX thread functions will need to utilize?

incldue the <pthreads.h> library and use pthread_create(), pthread_join(), pthread_detatch() and pthread_exit(). The mutex lock functions.

Make sure to compile with: "gcc -o program program.c -pthread" flag


Response.

> 5. You will need to perform testing on your client library.
>
>   - How will you test your client library?

I will make unit and functional tests.

>   - What will `echo_test` tell you?

it will test the functionality of the application

>   - How will you use the Google Test framework?

It will be used to make unit tests

>   - How will you incorporate testing at every stage of development?

Basically I'll plan out all the functions I need for a working product, and then "fill in the blanks" for each one to test if it works, that way I can just focus on individual components of the application.

> 6. You will need to create an user application that takes advantage of the
>    pub/sub system.
>
>   - What distributed and parallel application do you plan on building?

I plan to make a chat applciation.

>   - How will utilize the client library?

The topic will be the chatroom and users can publish messages and receive them using the client library.

>   - What topics will you need?

One topic for the chatroom, or else multiple topics if I want multiple chatrooms.

>
>   - What callbacks will you need?

I will need callback functions to display messages to the user in an organized, neat fashion.


>   - What additional threads will you need?

A thread will be needed for each message a user sendss.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 03]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project03.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
